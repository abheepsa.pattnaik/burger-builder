import Modal from "./Modal";
import {shallow} from "enzyme";
import {configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from "react";

configure({adapter: new Adapter()});

describe('Modal', () => {
  it('should ', () => {
    const modalTag = shallow(<Modal/>);
    const divTag = modalTag.find('div');
    expect(divTag.length).toBe(1);
  });
});