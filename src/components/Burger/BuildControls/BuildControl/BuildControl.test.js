import React from "react";
import BuildControl from "./BuildControl";
import {configure, mount} from "enzyme";
import Adapter from 'enzyme-adapter-react-16';

configure({adapter: new Adapter()});

describe('BuildControl', () => {
  describe('Add and Remove controls to be present', () => {
    it('should have more and less buttons', () => {
      const buildControlTag = mount(<BuildControl/>);
      const buttonTags = buildControlTag.find('button');
      expect(buttonTags.length).toBe(2);
      expect(buttonTags.at(0).text()).toBe('-');
      expect(buttonTags.at(1).text()).toBe('+');
    });
  });

  it('should reduce the ingredient on click of - button', () => {
    const props = {
      ingredients: {
        salad: 1,
        bacon: 0,
        cheese: 0,
        meat: 0,
      },
      totalPrice: 10,
      removed: jest.fn(),
    };
    const buildControlTag = mount(<BuildControl {...props}/>);
    const buttonTags = buildControlTag.find('button');
    buttonTags.at(0).simulate('click');
    expect(props.removed).toHaveBeenCalled();
  });

  it('should not reduce the ingredient on click of + button', () => {
    const props = {
      ingredients: {
        salad: 1,
        bacon: 0,
        cheese: 0,
        meat: 0,
      },
      totalPrice: 10,
      removed: jest.fn(),
    };
    const buildControlTag = mount(<BuildControl {...props}/>);
    const buttonTags = buildControlTag.find('button');
    buttonTags.at(1).simulate('click');
    expect(props.removed).not.toHaveBeenCalled();
  });

  it('should add the ingredient on click of + button', () => {
    const props = {
      ingredients: {
        salad: 1,
        bacon: 0,
        cheese: 0,
        meat: 0,
      },
      totalPrice: 10,
      added: jest.fn(),
    };
    const buildControlTag = mount(<BuildControl {...props}/>);
    const buttonTags = buildControlTag.find('button');
    buttonTags.at(1).simulate('click');
    expect(props.added).toHaveBeenCalled();
  });

  it('should not add the ingredient on click of - button', () => {
    const props = {
      ingredients: {
        salad: 1,
        bacon: 0,
        cheese: 0,
        meat: 0,
      },
      totalPrice: 10,
      added: jest.fn(),
    };
    const buildControlTag = mount(<BuildControl {...props}/>);
    const buttonTags = buildControlTag.find('button');
    buttonTags.at(0).simulate('click');
    expect(props.added).not.toHaveBeenCalled();
  });
});