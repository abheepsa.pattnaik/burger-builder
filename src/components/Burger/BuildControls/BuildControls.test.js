import React from "react";
import BuildControls from "./BuildControls";
import {shallow} from "enzyme";
import {configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({adapter: new Adapter()});

describe('BuildControls', () => {
  it('should render the BuildControls component', () => {
    const props = {
      price:10,
      disabled:[
        'salad',
        'bacon',
      ]
    };
    const buildControls = shallow(<BuildControls {...props}/>);
    const buttonTag = buildControls.find('button');
    expect(buttonTag.length).toBe(1);
    expect(buttonTag.at(0).text()).toBe('ORDER NOW');
  });
});