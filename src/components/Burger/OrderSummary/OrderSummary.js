import React from 'react';
import Aux from "../../../hoc/Aux";
import Button from "../../UI/Button/Button";

const OrderSummary = (props) => {
  const ingredientSummary = Object.keys(props.ingredients)
    .map((ingredientKey, i) =>
      <li key={i}>
       <span style={{textTransform: 'capitalize'}}
       >
          {ingredientKey}
          </span>
        :{props.ingredients[ingredientKey]}
      </li>
    );

  return (
    <Aux>
      <h3>Your Order</h3>
      <p>Burger with ingredients as :</p>
      <ul>
        {ingredientSummary}
      </ul>
      <p><strong>Total Price:{props.price.toFixed(2)}</strong></p>
      <p>Continue to checkout</p>
      <Button
        btnType='Danger'
        clicked={props.purchaseCancelled}
      >CANCEL
      </Button>
      <Button
        btnType='Success'
        clicked={props.purchaseContinued}>CONTINUE
      </Button>
    </Aux>)
};

export default OrderSummary;