import {shallow} from "enzyme/build";
import Aux from "../../hoc/Aux";
import React from "react";
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

describe('Layout', () => {
  it('should have the Aux component', () => {
    const burgerBuilderTag = shallow(<Aux/>);
    expect(burgerBuilderTag.length).toBe(1);
  });
});