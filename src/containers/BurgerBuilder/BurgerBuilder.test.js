import React from "react";
import Aux from "../../hoc/Aux";
import {shallow} from "enzyme";
import {configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Burger from "../../components/Burger/Burger";
import BurgerBuilder from "./BurgerBuilder";
import BuildControls from "../../components/Burger/BuildControls/BuildControls";

configure({adapter: new Adapter()});

describe('BurgerBuilder', () => {

  describe('render component', () => {
    it('should have all the components', () => {
      const burgerBuilderTag = shallow(<BurgerBuilder/>);
      const burgerTag = burgerBuilderTag.find(Burger);
      const burgerControlsTag = burgerBuilderTag.find(BuildControls);
      const auxTag = burgerBuilderTag.find(Aux);
      expect(auxTag.length).toBe(1);
      expect(burgerTag.length).toBe(1);
      expect(burgerControlsTag.length).toBe(1);
    });
  });
});